import {BuilderHUD} from './modules/BuilderHUD'
import {spawnGltfX} from './modules/SpawnerFunctions'

const Deer = new Entity()
Deer.addComponent(new GLTFShape("models/Deerfloor.glb"))
Deer.addComponent(new Transform({ position: new Vector3(8, 0, 8)}))
engine.addEntity(Deer)

const hud:BuilderHUD =  new BuilderHUD()
hud.attachToEntity(Deer)

let tree1Shape = new GLTFShape('models/tree1/tree1.glb')
let tree1 = spawnGltfX(tree1Shape, 6,0,6,	0,0,0,		1,1,1)    